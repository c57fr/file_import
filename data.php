<?php
    require 'filters.php';
?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Données</title>

  <style>
  html,
  body {
    box-sizing: border-box;

    min-height: 100vh;
    min-width: 100vw;
  }

  body {
    display: flex;

    flex-direction: column;

    align-items: center;
    justify-content: center;

    background: #000;
    color: #ffffff;
  }

  .title {
    font-size: 1.2rem;
  }

  .table-fill {
    background: white;

    border-radius: 3px;
    border-collapse: collapse;

    height: 320px;
    margin: auto;
    padding: 5px;
    max-width: 600px;
    width: 100%;

    box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
  }

  th {
    color: #D5DDE5;
    ;
    background: #1b1e24;

    border-bottom: 4px solid #9ea7af;
    border-right: 1px solid #343a45;

    font-size: 1rem;
    font-weight: 100;

    padding: 24px;

    text-align: left;
    text-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);

    vertical-align: middle;
  }

  th:first-child {
    border-top-left-radius: 3px;
  }

  th:last-child {
    border-top-right-radius: 3px;
    border-right: none;
  }

  tr {
    border-top: 1px solid #C1C3D1;
    color: #666B85;
  }

  tr:hover td {
    background: #4E5066;
    color: #FFFFFF;
    border-top: 1px solid #22262e;
  }

  tr:first-child {
    border-top: none;
    border-bottom: none;
  }

  tr:nth-child(odd) td {
    background: #EBEBEB;
  }

  tr:nth-child(odd):hover td {
    background: #4E5066;
  }

  tr:last-child td:first-child {
    border-bottom-left-radius: 3px;
    border-bottom-right-radius: 3px;
  }

  td {
    background: #FFFFFF;

    padding: 20px;

    text-align: left;
    vertical-align: middle;

    font-weight: 300;
    font-size: 0.8rem;

    border-right: 1px solid #C1C3D1;
  }

  td:last-child {
    border-right: 0;
  }

  td.text-left,
  th.text-left {
    text-align: left;
  }
  </style>
</head>

<body>
  <h1 class="title">Voici le résultat des données :</h1>
  <div>
    <table class="table-fill">
      <thead>
        <tr>
          <th class="text-left">Nom</th>
          <th class="text-left">Prénom</th>
          <th class="text-left">Date de naissance</th>
          <th class="text-left">Inscrit le</th>
        </tr>
      </thead>
      <tbody class="table-hover">
        <?php
        foreach ($users as $user) {
        ?>
        <tr>
          <td class="text-left"><?= $user['lastname'] ?></td>
          <td class="text-left"><?= $user['firstname'] ?></td>
          <td class="text-left"><?= formatDate($user['birthday'], 'd/m/Y H:i:s') ?></td>
          <td class="text-left"><?= formatDate($user['register_at']) ?></td>
        </tr>
        <?php
        }
        ?>
      </tbody>
    </table>
  </div>

</body>

</html>