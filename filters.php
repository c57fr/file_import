<?php

// Permet de filtrer les date au format qu'on lui passe en paramètre
function formatDate(string $date, string $format = 'd/m/Y') : string {
    $date = date_create($date);
    return date_format($date, $format);
}
