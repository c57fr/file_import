<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>File Upload</title>

  <style>
    html,
    body {
      box-sizing: border-box;

      min-height: 100vh;
      min-width: 100vw;
    }

    body {
      display: flex;

      align-items: center;
      justify-content: center;

      background: #000;
    }

    .input-file-container {
      position: relative;

      width: 225px;
    }

    .input-file-label {
      display: block;

      padding: 14px 45px;

      background: #39D2B4;
      color: #fff;

      font-size: 1em;

      transition: all .4s;

      cursor: pointer !important;
    }

    .input-file {
      position: absolute;
      top: 0;
      left: 0;

      width: 225px;
      padding: 14px 0;

      opacity: 0;

      cursor: pointer;
    }

    .input-file:hover+.input-file-label,
    .input-file:focus+.input-file-label,
    .input-file-label:hover,
    .input-file-label:focus {
      background: #34495E;
      color: #39D2B4;
    }

    .form {
      text-align: center;
    }

    .form-submit {
      margin: 10px auto;
      padding: 10px 20px;

      background: #F24B18;
      color: #fff;

      cursor: pointer;

      border: 0;
      transition: all 0.3s ease;
    }

    .form-submit:hover {
      background: #773E14;
      color: #F24B18;
    }
  </style>
</head>

<body>
  <!--On précise que le formulaire est de type POST-->
  <!--Et qu'on va recevoir des fichiers-->
  <form action="upload.php" class="form" method="post" enctype="multipart/form-data">
    <div class="input-file-container">
      <input class="input-file" id="file-upload" type="file" name="file-upload">
      <label for="file-upload" class="input-file-label">Choisir un fichier CSV...</label>
    </div>
    <button type="submit" class="form-submit" name="submit">Valider</button>
  </form>
</body>

</html>