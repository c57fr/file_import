<?php

/**
 * (ɔ) Online FORMAPRO - File-Import - GC7 - 2022-2023.
 */

require_once 'tools/helpers.php';

// On vérifie que les données du formulaire sont bien présentes
// Et que le fichier n'est pas vide
// define('IMPORT', 'import.csv');
// aff(IMPORT);
// exit();
if (
	!isset($_FILES['file-upload'])
	|| 0 === $_FILES['file-upload']['size']
) {
	backTo('index.php');
}

// On initialise les données
$users = [];
// On essaye d'ouvrir le fichier temporaire que PHO a généré avec notre fichier
// $handle va contneir un "stream" de fichier
if (($handle = fopen($_FILES['file-upload']['tmp_name'], 'r')) !== false) {
	// On lit ce stream avec la fonction fgetcsv qui va venir récupérer les lignes
	// Une par une. On boucle pour que toutes les lignes soit lues
	while (($data = fgetcsv($handle, 1000)) !== false) {
		// On enregistre dans notre variable les colonnes qui nous intéressent
		$users[] = [
			'lastname'         => $data[1],
			'firstname'        => $data[2],
			'birthday'    => $data[3],
			'register_at' => $data[4],
		];
	}
	// On ferme le stream une fois que l'on en a plus besoin
	fclose($handle);
} else {
	// Si le stream ne peut pas être ouvert (problème de droit, illisibilité, etc)
	// On retourne à l'accueil
	backTo('index.php');
}

// On supprime le premier élément du tableau qui contient l'en-tête du CSV
// il s'agit d'une méthode par référence, il n'y a pas besoin de stocker le retour de la fonction
// Elle modifie la variable directement
array_shift($users);

require 'data.php';
