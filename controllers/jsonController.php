<?php

function getArrayFromJson($filename = 'import'): array
{
	// $file     = fopen($_FILES['file-upload']['name'], 'r');
	$file     = fopen($filename.'.csv', 'r');
	$line     = fgetcsv($file);
	$nbFields = count($line);
	for ($i = 0; $i < $nbFields; ++$i) {
		$tableHead[] = trim($line[$i]);
	}

	while ($line = fgetcsv($file)) {
		$lines[] = $line;
	}

	return [$filename, $nbFields, $tableHead, $lines];
}