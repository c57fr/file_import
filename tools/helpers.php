<?php

/**
 * (ɔ) Online FORMAPRO - File-Import - GC7 - 2022-2023.
 */

$GLOBALS['ROOT_DOCUMENT'] = realpath($_SERVER['DOCUMENT_ROOT']);

function getUri()
{
	return explode('?', $_SERVER['REQUEST_URI'])[0];
}

function controllers(string $name): string
{
	return getPath('controllers') . $name . 'Controller.php';
}

function views(string $name): string
{
	return getPath('views') . $name . 'View.twig';
}

function getPath(string $type): string
{
	return $GLOBALS['ROOT_DOCUMENT'] . DIRECTORY_SEPARATOR . $type . DIRECTORY_SEPARATOR;
}
function backTo(string $file): bool
{
	header('Location: ' . $file);
	exit;
}

function aff($var, $txt = null)
{
	echo '<a title=' . debug_backtrace()[0]['file'] . '&nbsp;-&nbsp;Line&nbsp;' . debug_backtrace()[0]['line'] . '><pre>' . (($txt) ? $txt . ' : ' : '');
	var_dump($var);
	echo '</pre></a>';
}
// aff(debug_backtrace());
